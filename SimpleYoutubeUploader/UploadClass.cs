﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleYoutubeUploader
{
    /// <summary>
    /// YouTube Data API v3 sample: upload a video.
    /// Relies on the Google APIs Client Library for .NET, v1.7.0 or higher.
    /// See https://developers.google.com/api-client-library/dotnet/get_started
    /// </summary>
    public class UploadClass
    {
        public static String title, description, tags, videoCategory, videoStatus, dateipfad;
        static public void MainFunction()
        {
            Console.WriteLine("YouTube Data API: Upload Video");
            Console.WriteLine("==============================");

            try
            {
                new UploadClass().Run().Wait();
            }
            catch (AggregateException ex)
            {
                foreach (Exception e in ex.InnerExceptions)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }

            Console.WriteLine("Press any key to continue...");
            Console.Read();
        }

        private async Task Run()
        {
            string[] scopes = new string[] { "https://www.googleapis.com/auth/youtube.upload" };


            Console.WriteLine("Requesting authorization");
            UserCredential credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                new ClientSecrets
                {
                    ClientId = "748844387786-h020ciubnvsjtkklk60s19a7b8phvjrc.apps.googleusercontent.com",
                    ClientSecret = "0SXjNCXAKC_tUdxrubTwEdV0"
                },
                 scopes,
                 "user",
                 CancellationToken.None).Result;
            Console.WriteLine("Authorization granted or not required (if the saved access token already available)");


            YouTubeService youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
            });

            Video video = new Video();
            video.Snippet = new VideoSnippet();
            video.Snippet.Title = title;
            video.Snippet.Description = description;
            video.Snippet.Tags = new string[] { tags, tags }; //Needs still work
            video.Snippet.CategoryId = videoCategory; // See https://developers.google.com/youtube/v3/docs/videoCategories/list
            video.Status = new VideoStatus();
            video.Status.PrivacyStatus = videoStatus; // "unlisted" or "private" or "public"
            string filePath = dateipfad; // Replace with path to actual movie file.

            using (FileStream fileStream = new FileStream(filePath, FileMode.Open))
            {
                VideosResource.InsertMediaUpload videosInsertRequest = youtubeService.Videos.Insert(video, "snippet,status", fileStream, "video/*");
                videosInsertRequest.ProgressChanged += videosInsertRequest_ProgressChanged;
                videosInsertRequest.ResponseReceived += videosInsertRequest_ResponseReceived;

                await videosInsertRequest.UploadAsync();
            }
        }
        void videosInsertRequest_ProgressChanged(Google.Apis.Upload.IUploadProgress progress)
        {
            
            switch (progress.Status)
            {

                case UploadStatus.Uploading:
                    //TODO Show in GUI
                    Console.WriteLine("{0} bytes sent.", progress.BytesSent);
                    break;

                case UploadStatus.Failed:
                    Console.WriteLine("An error prevented the upload from completing.\n{0}", progress.Exception);
                    break;
            }
        }
              
        void videosInsertRequest_ResponseReceived(Video video)
        {
            //TODO Show in GUI
            Console.WriteLine("Video id '{0}' was successfully uploaded. "+video.Status, video.Id);
        }
    }
}