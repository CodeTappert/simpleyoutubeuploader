﻿using Microsoft.Win32;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Threading;

using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.Util.Store;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using System;

namespace SimpleYoutubeUploader
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool fileExists = false, statusSet = false, categorySet = false, titleSet = false, descriptionSet = false,tagsSet = false;
        private UploadClass uploadClass;

        public MainWindow()
        {
            uploadClass = new UploadClass();
            InitializeComponent();
        }

        private void btn_File_Click(object sender, RoutedEventArgs e)
        {
            string filePath = "";
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = false;
            openFileDialog.InitialDirectory = "C:\\";

            if (openFileDialog.ShowDialog() == true)
            {
                filePath = openFileDialog.FileName;
                textBox_file.Text = filePath;
            }
        }

        private void TextBox_Description_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (textBox_Description.Text.Length != 0)
            {
                descriptionSet = true;
                UploadClass.description = textBox_Description.Text;
            }
            else
            {
                descriptionSet = false;
            }
        }

        private void TextBox_file_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (File.Exists(textBox_file.Text))
            {
                fileExists = true;
                UploadClass.dateipfad = textBox_file.Text;
            }
            else
            {
                fileExists = false;
            }
        }

        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            if (CheckIfEverythingIsOk())
            {
                FileInfo fileInfo = new FileInfo(textBox_file.Text);
                long fileSize = fileInfo.Length;
                btnStart.IsEnabled = false;
                Thread thread = new Thread(new ThreadStart(UploadClass.MainFunction));
                thread.Start();
                thread.Join();
                btnStart.IsEnabled = true;

                
            }
        }

        private void TextBox_Tags_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (textBox_Tags.Text.Length != 0)
            {
                tagsSet = true;
                UploadClass.tags = textBox_Tags.Text;
            }
            else
            {
                tagsSet = false;
            }
        }

        private bool CheckIfEverythingIsOk()
        {
            if (!fileExists)
            {
                MessageBox.Show("Keine Datei ausgewählt!");
                    return false;
            }
            if (!titleSet)
            {
                MessageBox.Show("Kein Titel festgelegt!");
                    return false;
            }
            if (!descriptionSet)
            {
                MessageBox.Show("Keine Beschreibung eingetragen!");
                return false;
            } if (!tagsSet)
            {
                MessageBox.Show("Keine Tags eingetragen!");
                    return false;
            }
            if (!statusSet)
            {
                MessageBox.Show("Es wurden keine Privatsphäre Einstellungen vorgenommen!");
                    return false; }
            if (!categorySet)
            {
                MessageBox.Show("Es wurde keine Kategorie ausgewählt!");
                    return false; }
            return true;
        }

        private void TextBox_Title_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (textBox_Title.Text.Length != 0)
            {
                titleSet = true;
                UploadClass.title = textBox_Title.Text;

            }
            else
            {
                titleSet = false;
            }
        }

        private void ComboBoxCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            categorySet = true;
            switch (comboBoxCategory.SelectedIndex)
            {
                case 0:
                    UploadClass.videoCategory = "1";
                    break;
                case 1:
                    UploadClass.videoCategory = "2";
                    break;
                case 2:
                    UploadClass.videoCategory = "10";
                    break;
                case 3:
                    UploadClass.videoCategory = "15";
                    break;
                case 4:
                    UploadClass.videoCategory = "17";
                    break;
                case 5:
                    UploadClass.videoCategory = "19";
                    break;
                case 6:
                    UploadClass.videoCategory = "20";
                    break;
                case 7:
                    UploadClass.videoCategory = "22";
                    break;
                case 8:
                    UploadClass.videoCategory = "23";
                    break;
                case 9:
                    UploadClass.videoCategory = "24";
                    break;
                case 10:
                    UploadClass.videoCategory = "25";
                    break;
                case 11:
                    UploadClass.videoCategory = "26";
                    break;
                case 12:
                    UploadClass.videoCategory = "27";
                    break;
                case 13:
                    UploadClass.videoCategory = "28";
                    break;
            }
        }

        private void ComboBoxPrivat_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            statusSet = true;
            if (comboBoxPrivat.SelectedIndex == 0)
            {
                UploadClass.videoStatus = "private";
            }
            if (comboBoxPrivat.SelectedIndex == 1)
            {
                UploadClass.videoStatus = "unlisted";
            }
            if (comboBoxPrivat.SelectedIndex == 2)
            {
                UploadClass.videoStatus = "public";
            }
        }

        
    }
}

